" =============== Settings ===============
set autowrite       " Automatically :write before running commands
set expandtab       " Insert spaces when tab is pressed
set exrc            " Enable per-directory .vimrc files
set history=50      " Search and command history to 50
set hlsearch        " Highlight found searches
set ignorecase      " Ignore case while searching
set iminsert=0      " Disable capslock
set incsearch       " Do incremental searching
set laststatus=2    " Always display the status line
set mouse=a         " Enable mouse support in all modes
set nobackup        " Don't create a backup file before overwriting
set noswapfile      " http://robots.thoughtbot.com/post/18739402579/global-gitignore#comment-458413287
set nowrap          " Prevent word wrapping on long lines
set nowritebackup   " Don't create a backup before writing
set number          " Show line numbers
set relativenumber  " Show line numbers relative to cursor
set ruler           " Show the cursor position all the time
set secure          " Disable unsafe commands in local .vimrc files
set shiftround      " Round indent to multiple of 'shiftwidth'
set shiftwidth=2    " '>>'/'<<' to use 2 spaces
set showcmd         " Display incomplete commands
set showmode        " Show the current mode
set smartcase       " Only do case sensitive searching when there is a capitalized letter
set splitbelow      " Pen new split panes to right and bottom, which feels more natural
set splitright      " Vertical splits with open to the right
set tabstop=2       " Softtabs, 2 spaces

" =============== Wildmenu completion ===============
set wildmenu
set wildmode=list:longest
set wildmode=list:full
set wildignore+=*.hg,*.git,*.svn                  " Version control
set wildignore+=*.aux,*.out,*.toc                 " LaTeX intermediate files
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg    " binary images
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest  " compiled object files
set wildignore+=*.spl                             " compiled spelling word lists
set wildignore+=*.sw?                             " Vim swap files
set wildignore+=*.DS_Store                        " OSX folder meta data
set wildignore+=*.luac                            " Lua byte code
set wildignore+=migrations                        " Django migrations
set wildignore+=go/pkg                            " Go static files
set wildignore+=go/bin                            " Go bin files
set wildignore+=go/bin-vagrant                    " Go bin-vagrant files
set wildignore+=*.pyc                             " Python byte code
set wildignore+=*.orig                            " Merge resolution files

" =============== Dictionary ===============
set spelllang=en_us
set spellsuggest=best,3
set dictionary+=/usr/share/dict/words,
set dictionary+=/usr/share/dict/american-english
set dictionary+=/usr/share/dict/web2,
set dictionary+=/usr/share/dict/propernames.gz
set dictionary+=/usr/share/dict/connectives.gz
set dictionary+=/usr/share/dict/web2a.gz
set spellfile=~/.nvim/dict.custom.utf-8.add

" =============== FileType ===============
au FileType go setlocal noexpandtab
au FileType make setlocal noexpandtab               " Use tab (\t) character
au FileType markdown,gitcommit set spell            " Enable spell checking

au BufNewFile,BufRead *.go setlocal noet ts=2 sw=2  " Use tab (\t), '>>'/'<<' use 2 spaces, treat tabs as 2 spaces in commands
au BufWinEnter,WinEnter term://* startinsert        " Start terminal in insert mode
au BufLeave term://* stopinsert                     " Exit terminal in normal mode

" =============== Misc ===============
let $NVIM_TUI_ENABLE_TRUE_COLOR=1 " Enable true colors (16 million colors in terminal)

" =============== Bindings ===============
let mapleader = "\<SPACE>"
nnoremap <leader><leader> <SPACE>

" Disable Ex mode
nnoremap Q :<Nop>

" Strip Trailing Whitespace
nnoremap <Leader>kw :%s/\s\+$//e<CR>:noh<CR>

" Allow split switching using C-h/j/k/k in terminal
tnoremap <C-h> <C-\><C-n><C-w>h
tnoremap <C-j> <C-\><C-n><C-w>j
tnoremap <C-k> <C-\><C-n><C-w>k
tnoremap <C-l> <C-\><C-n><C-w>l

" Load plugins
if filereadable(expand("~/.config/nvim/nvimrc.bundles"))
  source ~/.config/nvim/nvimrc.bundles
endif

" Local config
if filereadable(expand("~/.config/nvim/nvimrc.local"))
  source ~/.config/nvim/nvimrc.local
endif

