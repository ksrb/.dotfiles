# =============== ENV variables ===============
# Location of on-my-zsh
export ZSH=$HOME/.oh-my-zsh

export EDITOR='vim'

export GOPATH=$HOME/gocode
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOBIN
export PATH=$PATH:$HOME/.bin
export PATH=$PATH:$HOME/bin

# Relative inclusions to PATH
export PATH=$PATH:./node_modules/.bin
export PATH=$PATH:./bin
export PATH=$PATH:./.bin

# Deduplicate path in tmux
# see https://github.com/thoughtbot/dotfiles/issues/443
export -U PATH

# =============== ZSH settings ===============
# Theme
ZSH_THEME="robbyrussell"
plugins=(git docker)

# =============== Aliases ===============
alias vi="nvim"
alias vim="nvim"
# Hide dotfiles
alias hidden!="defaults write com.apple.finder AppleShowAllFiles TRUE && killall Finder"
# Show dotfiles
alias hidden="defaults write com.apple.finder AppleShowAllFiles FALSE && killall Finder"
# Copy public key to clipboard
alias pubkey="more ~/.ssh/id_rsa.pub | pbcopy | echo '=> Public key copied to pasteboard.'"

# =============== Functions ===============
# Directory shortcuts
p (){ cd ~/projects/$1; }
compctl -W $HOME/projects/ -/ p

tcase (){ cd ~/test_cases/$1; }
compctl -W $HOME/test_cases/ -/ tcase

# Determine local IP address
ips () {
  ifconfig | grep "inet " | awk '{ print $2 }'
}

extract () {
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)        tar xjf $1        ;;
      *.tar.gz)         tar xzf $1        ;;
      *.bz2)            bunzip2 $1        ;;
      *.rar)            unrar x $1        ;;
      *.gz)             gunzip $1         ;;
      *.tar)            tar xf $1         ;;
      *.tbz2)           tar xjf $1        ;;
      *.tgz)            tar xzf $1        ;;
      *.zip)            unzip $1          ;;
      *.Z)              uncompress $1     ;;
      *)                echo "'$1' cannot be extracted via extract()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Local Settings 
# Needs to be sourced before sourcing oh-my-zsh or else additional plugins
# cannot be appended
[[ -s "$HOME/.zshrc.local" ]] && source "$HOME/.zshrc.local"

# Initialize oh-my-zsh
source $ZSH/oh-my-zsh.sh
