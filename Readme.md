
# Dotfiles

Lantern Credit Team Dotfiles

## Install
```bash
$ curl -L https://raw.lcode.io/tools/.dotfiles/master/dotfiles | bash -s install all
```

## License

(c) 2016 Lantern Credit LLC, All Rights Reserved
